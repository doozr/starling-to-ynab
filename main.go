package main

import (
	"encoding/csv"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)

// CSVRecord is a sequence of string values
type CSVRecord []string

func ynabFilename(starlingFilename string) string {
	if strings.HasSuffix(starlingFilename, ".csv") {
		extIndex := strings.LastIndex(starlingFilename, ".")
		starlingFilename = starlingFilename[:extIndex]
	}
	return starlingFilename + "-YNAB.csv"
}

func parseFloatCurrencyToUnit(value string) (amount int64, err error) {
	if value == "" {
		return 0, nil
	}

	decimalIndex := strings.Index(value, ".")

	if decimalIndex == -1 {
		amount, err = strconv.ParseInt(value, 10, 64)
		amount = amount * 100
		return
	}

	var integer int64
	integerValue := value[:decimalIndex]
	if len(integerValue) > 0 {
		integer, err = strconv.ParseInt(integerValue, 10, 64)
		if err != nil {
			return
		}
	}

	var decimal int64
	decimalValue := value[decimalIndex+1:]
	if len(decimalValue) > 2 {
		decimalValue = decimalValue[:2]
	}

	if len(decimalValue) > 0 {
		decimal, err = strconv.ParseInt(decimalValue, 10, 64)
		if err != nil {
			return
		}
	}

	if integer < 0 {
		decimal = 0 - decimal
	}

	amount = (integer * 100) + decimal
	return
}

func starlingToYNAB(starlingRecord CSVRecord) (ynabRecord CSVRecord, err error) {
	if len(starlingRecord) != 6 {
		err = fmt.Errorf("Expected 6 fields, got %d", len(starlingRecord))
		return
	}

	value, err := parseFloatCurrencyToUnit(starlingRecord[4])
	if err != nil {
		return
	}

	whole := value / 100
	part := value % 100

	ynabRecord = make(CSVRecord, 5, 5)
	copy(ynabRecord, starlingRecord[0:3])

	if value >= 0 {
		ynabRecord[4] = fmt.Sprintf("%d.%02d", whole, part)
	} else {
		ynabRecord[3] = fmt.Sprintf("%d.%02d", -whole, -part)
	}
	return
}

func main() {
	if len(os.Args) < 2 {
		fmt.Println("Usage: starling-to-ynab <FILE>")
		return
	}

	inputFilename := os.Args[1]
	inputFile, err := os.Open(inputFilename)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	defer inputFile.Close()

	outputFilename := ynabFilename(inputFilename)
	outputFile, err := os.OpenFile(outputFilename, os.O_WRONLY|os.O_TRUNC|os.O_CREATE, 0644)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	defer outputFile.Close()

	reader := csv.NewReader(inputFile)
	writer := csv.NewWriter(outputFile)
	defer writer.Flush()

	err = writer.Write([]string{"Date", "Payee", "Memo", "Outflow", "Inflow"})

	fmt.Printf("Reading Starling statement from %s\n", inputFilename)
	fmt.Printf("Writing YNAB file to %s\n", outputFilename)
	for {
		starlingRecord, err := reader.Read()
		if err == io.EOF {
			break
		}

		if err != nil {
			fmt.Println(err)
			break
		}

		// Skip header row
		if starlingRecord[0] == "Date" {
			continue
		}

		fmt.Printf("Read Starling record: %q\n", starlingRecord)
		ynabRecord, err := starlingToYNAB(starlingRecord)
		if err != nil {
			fmt.Println(err)
			continue
		}

		err = writer.Write(ynabRecord)
		if err != nil {
			fmt.Println(err)
			continue
		}

		fmt.Printf("Wrote YNAB record: %q\n", ynabRecord)
	}

	fmt.Printf("Finished writing YNAB file to %s\n", outputFilename)
}
