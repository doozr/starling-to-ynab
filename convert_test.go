package main

import (
	"reflect"
	"testing"
)

func TestStarlingToYNAB(t *testing.T) {
	tests := []struct {
		name     string
		input    CSVRecord
		expected CSVRecord
		err      bool
	}{{
		name:  "not enough fields produces error",
		input: CSVRecord{"20/01/2018", "Me"},
		err:   true,
	}, {
		name:  "too manu fields produces error",
		input: CSVRecord{"20/01/2018", "Me"},
		err:   true,
	}, {
		name:     "valid deposit parses",
		input:    CSVRecord{"20/01/2018", "A Company", "Paycheck", "FASTER PAYMENT", "1000.00", "1000.00"},
		expected: CSVRecord{"20/01/2018", "A Company", "Paycheck", "", "1000.00"},
	}, {
		name:     "valid withdrawal parses",
		input:    CSVRecord{"20/01/2018", "A Shop", "Food", "DEBIT CARD", "-12.99", "987.01"},
		expected: CSVRecord{"20/01/2018", "A Shop", "Food", "12.99", ""},
	}, {
		name:  "invalid value produces error",
		input: CSVRecord{"20/01/2018", "A Shop", "Food", "DEBIT CARD", "nope", "987.01"},
		err:   true,
	}, {
		name:     "extra precision is truncated",
		input:    CSVRecord{"20/01/2018", "A Shop", "Food", "DEBIT CARD", "-12.99435673", "987.01"},
		expected: CSVRecord{"20/01/2018", "A Shop", "Food", "12.99", ""},
	}, {
		name:     "rounding error does not happen",
		input:    CSVRecord{"20/01/2018", "A Bank", "Transfer", "DEBIT CARD", "595.05", "595.05"},
		expected: CSVRecord{"20/01/2018", "A Bank", "Transfer", "", "595.05"},
	}, {
		name:     "missing decimal portion is read as zero",
		input:    CSVRecord{"20/01/2018", "A Shop", "Food", "DEBIT CARD", "-12.", "987.01"},
		expected: CSVRecord{"20/01/2018", "A Shop", "Food", "12.00", ""},
	}, {
		name:     "integer value is processed as zero decimal portion",
		input:    CSVRecord{"20/01/2018", "A Shop", "Food", "DEBIT CARD", "-12.", "987.01"},
		expected: CSVRecord{"20/01/2018", "A Shop", "Food", "12.00", ""},
	}, {
		name:     "missing integer part is read as zero",
		input:    CSVRecord{"20/01/2018", "A Shop", "Food", "DEBIT CARD", ".99", "987.01"},
		expected: CSVRecord{"20/01/2018", "A Shop", "Food", "", "0.99"},
	}, {
		name:     "missing value is read as zero incoming",
		input:    CSVRecord{"20/01/2018", "A Shop", "Food", "DEBIT CARD", "", "987.01"},
		expected: CSVRecord{"20/01/2018", "A Shop", "Food", "", "0.00"},
	}, {
		name:     "multiple decimal points truncated",
		input:    CSVRecord{"20/01/2018", "A Shop", "Food", "DEBIT CARD", "-12.99.23", "987.01"},
		expected: CSVRecord{"20/01/2018", "A Shop", "Food", "12.99", ""},
	}}

	for _, test := range tests {
		actual, err := starlingToYNAB(test.input)

		if test.err && err == nil {
			t.Errorf("%s: Expected error but got none", test.name)
			continue
		}

		if !test.err && err != nil {
			t.Errorf("%s: No error expected but got %v", test.name, err)
		}

		if !test.err && !reflect.DeepEqual(actual, test.expected) {
			t.Errorf("%s: Expected %v but got %v", test.name, test.expected, actual)
			continue
		}
	}
}
