package main

import "testing"

func TestYNABName(t *testing.T) {
	tests := []struct {
		name     string
		input    string
		expected string
	}{{
		name:     "csv extension is retained",
		input:    "StarlingStatement_Jan-2018.csv",
		expected: "StarlingStatement_Jan-2018-YNAB.csv",
	}, {
		name:     "missing csv extension is added",
		input:    "StarlingStatement_Jan-2018.txt",
		expected: "StarlingStatement_Jan-2018.txt-YNAB.csv",
	}}

	for _, test := range tests {
		actual := ynabFilename(test.input)

		if actual != test.expected {
			t.Errorf("%s: expected %s got %s", test.name, test.expected, actual)
		}
	}
}
